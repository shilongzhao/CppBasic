#include <iostream>

using namespace std;

int main() {
    // io
    cout << "Hello, World!" << endl;
//    int v1, v2;
//    cin >> v1 >> v2;
//    cout << "sum = " << v1 + v2 << endl;

    // while loop
    int i = 0, sum = 0;
    while (i < 100) {
        sum += i;
        i++;
    }
    cout << sum << endl;

    string book("hello!");
    cout << book << endl;

    double val = 1.1;
    double &rval = val;
    cout << rval << endl;

    double *p = &rval;
    cout << p << endl;
    cout << *p << endl;
}