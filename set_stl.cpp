//
// Created by ZhaoShilong on 25/06/2018.
//

#include <iostream>
#include <set>
using namespace std;

int main() {
    set<int> fooset;
    set<int>::iterator it;

    for (int i = 1; i <= 5; i++) {
        fooset.insert(i * 10);
        fooset.insert(i * 10);
    }
    it = fooset.find(20);
    fooset.erase(it);
    fooset.erase(fooset.find(30));

    for (it = fooset.begin(); it != fooset.end(); it++) {
        cout << ' ' << *it;
    }
    return 0;
}