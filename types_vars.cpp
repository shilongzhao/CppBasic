//
// Created by ZhaoShilong on 21/06/2018.
//
#include <iostream>
using namespace std;

int main() {
    int a;
    double b;
    char c;
    cin >> b;
    if (b > 100) {
        cout << "greater than 100" << endl;
    }
    else {
        cout << "not greater than 100" << endl;
    }

    double &ref = b;
    ref = 101;
    cout << "b = " << b << endl;

}