//
// Created by ZhaoShilong on 23/06/2018.
//
#include <iostream>

void zeroOut(int &a) {
    a = 0;
}

void reset(int *r) {
    *r = 0;
}
int main() {
    int x = 10;
    zeroOut(x);
    std::cout << x << std::endl;

    int y = 10;
    reset(&y);
    std::cout << y << std::endl;

}