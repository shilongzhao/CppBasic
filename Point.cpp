//
// Created by ZhaoShilong on 23/06/2018.
//
class Point {
private:
    int x, y;
public:
    Point(int x, int y): x(x), y(y) {}
    int getX() {
        return x;
    }
    int getY() {
        return y;
    }
};
