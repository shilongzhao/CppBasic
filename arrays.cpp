//
// Created by ZhaoShilong on 21/06/2018.
//

#include <iostream>
#include <map>

using namespace std;

int main() {
    string members[] = {"Marco", "Lorenzo"};
    cout << members->size() << endl;
    cout << members->length() << endl;
    for (int i = 0; i < 2; i++) {
        cout << members[i] << endl;
    }

    for (auto s: members) {
        cout << s << endl;
    }

    unsigned int scores[5] = {12, 23, 33, 43, 23};
    for (auto s: scores) {
        cout << s << endl;
    }
    map<int, int> stats;
    for (unsigned int s: scores) {
        stats[s]++;
    }
    for (auto it = stats.begin(); it != stats.end(); it++) {
        cout << it->first << " ==> " << it->second << endl;
    }
}