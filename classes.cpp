//
// Created by ZhaoShilong on 25/06/2018.
//
#include <iostream>
using namespace std;

class base {
public:
    virtual void printMsg() {
        cout << "hello from base" << endl;
    }
    void show() {
        cout << "show from base" << endl;
    }
};

class derived: public base {
public:
    void printMsg() {
        cout << "hello from derived" << endl;
    }
    void show() {
        cout << "show from derived" << endl;
    }
};

int main() {
    base *bptr;
    derived d;
    bptr = &d;

    bptr->printMsg();
    bptr->show();

    base &br = d;
    br.show();
    br.printMsg();
}