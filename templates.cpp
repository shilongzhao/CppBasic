//
// Created by ZhaoShilong on 25/06/2018.

#include <iostream>
using namespace std;

template <typename T>
T mymax(T x, T y) {
    return (x > y) ? x : y;
}

int main() {
    cout << mymax<int>(3, 7) << endl;
    cout << mymax<char>('a', 'g') << endl;
}