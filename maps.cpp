//
// Created by ZhaoShilong on 25/06/2018.
//
#include <iostream>
#include <set>
#include <map>

using namespace std;

int main() {
    map<string, size_t> word_count;
    set<string> word_set;

    string word;
    int i = 0;
    while (i++ < 5) {
        cin >> word;
        ++word_count[word];
        word_set.insert(word);
    }
    for (const auto &w: word_count) {
        cout << w.first << " occurs " << w.second << endl;
    }

    for (const auto &w: word_set) {
        cout << w << endl;
    }
}