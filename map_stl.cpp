//
// Created by ZhaoShilong on 25/06/2018.
//

#include <iostream>
#include <map>
using namespace std;
int main() {
    map<char, int> frequencies;
    frequencies['a'] = 100;
    frequencies['b'] = 200;
    frequencies['b']++;

    for (map<char, int>::iterator it = frequencies.begin(); it != frequencies.end(); it++ ) {
        cout << it->first << " ==> " << (*it).second << endl;
    }

    cout<< frequencies.size() << endl;

    string h = "helloworld!";
    map<char, int> stats;
    for (string::iterator it = h.begin(); it != h.end(); it++) {
        stats[*it]++;
    }

    for (map<char, int>::iterator it = stats.begin(); it != stats.end(); it++) {
        cout << it->first << " ==> " << (*it).second << endl;
    }

}