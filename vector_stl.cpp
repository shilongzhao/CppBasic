//
// Created by ZhaoShilong on 24/06/2018.
//

#include <vector>
#include <iostream>

using namespace std;

int main() {
    std::vector<int> v1;
    for (int i = 0; i < 10; i++) {
        v1.push_back(i);
    }

    for (auto it = v1.begin(); it != v1.end(); it++) {
        std::cout << ' ' << *it;
    }
    std::cout << std::endl;

    for (int &it : v1) {
        it *= it;
        std::cout << ' ' << it;
    }

    cout << endl;

    vector<string> v2  = {"b", "a", "mm"};
    for (string &s: v2) {
        cout << s << endl;
    }
}